<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <base href="https://winstonhoseadriggins.gitlab.io/hugo-anybodyhome/">

    <title>DW84 Inc LLC Foundation</title>

    <link
      rel="canonical"
      href="https://winstonhoseadriggins.gitlab.io/hugo-anybodyhome/post/go-programming-language/"
    />

    <link
      href=""
      rel="alternate"
      type="application/rss+xml"
      title="DW84 Inc LLC Foundation"
    />

    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"
    />

    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.6.0/styles/default.min.css"
    />
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.6.0/highlight.min.js">
    </script>
    <script>
      hljs.initHighlightingOnLoad();
    </script>

    <link
      rel="stylesheet"
      type="text/css"
      href="/css/styles.css"
    />
</head>

<section id="header">
  <h1>
    <a href="https://winstonhoseadriggins.gitlab.io/hugo-anybodyhome/">DW84 Inc LLC Foundation</a>
  </h1>
  <h2>## DW84 Inc LLC Foundation Corporate Mission is to Establish the Digital Identity Of Every Sentient Being Residing On Planet Earth! ##</h2>
</section>


<div id="post">
<section>
  <h2>Language and Locale Matching in Go</h2>
  <time>5. October, 2016</time>
</section>
<section>
  <article>
    <p>Marcel van Lohuizen</p>

<ul>
<li>Introduction
Marcel van Lohuizen</li>
</ul>

<p>Consider an application, such as a web site, with support for multiple languages
in its user interface.
When a user arrives with a list of preferred languages, the application must
decide which language it should use in its presentation to the user.
This requires finding the best match between the languages the application supports
and those the user prefers.
This post explains why this is a difficult decision and how Go can help.</p>

<ul>
<li>Language Tags</li>
</ul>

<p>Language tags, also known as locale identifiers, are machine-readable
identifiers for the language and/or dialect being used.
The most common reference for them is the IETF BCP 47 standard, and that is the
standard the Go libraries follow.
Here are some examples of BCP 47 language tags and the language or dialect they
represent.</p>

<p>.html matchlang/tags.html</p>

<p>The general form of the language tag is
a language code (“en”, “cmn”, “zh”, “nl”, “az” above)
followed by an optional subtag for script (“-Arab”),
region (“-US”, “-BE”, “-419”),
variants (“-oxendict” for Oxford English Dictionary spelling),
and extensions (“-u-co-phonebk” for phone-book sorting).
The most common form is assumed if a subtag is omitted, for instance
“az-Latn-AZ” for “az”.</p>

<p>The most common use of language tags is to select from a set of system-supported
languages according to a list of the user&rsquo;s language preferences, for example
deciding that a user who prefers Afrikaans would be best served (assuming
Afrikaans is not available) by the system showing Dutch. Resolving such matches
involves consulting data on mutual language comprehensibility.</p>

<p>The tag resulting from this match is subsequently used to obtain
language-specific resources such as translations, sorting order,
and casing algorithms.
This involves a different kind of matching. For example, as there is no specific
sorting order for Portuguese, a collate package may fall back to the sorting
order for the default, or “root”, language.</p>

<ul>
<li>The Messy Nature of Matching Languages</li>
</ul>

<p>Handling language tags is tricky.
This is partly because the boundaries of human languages are not well defined
and partly because of the legacy of evolving language tag standards.
In this section we will show some of the messy aspects of handling language tags.</p>

<p>__Tags_with_different_language_codes_can_indicate_the_same<em>language</em></p>

<p>For historical and political reasons, many language codes have changed over
time, leaving languages with an older legacy code as well as a new one.
But even two current codes may refer to the same language.
For example, the official language code for Mandarin is “cmn”, but “zh” is by
far the most commonly used designator for this language.
The code “zh” is officially reserved for a so called macro language, identifying
the group of Chinese languages.
Tags for macro languages are often used interchangeably with the most-spoken
language in the group.</p>

<p>_Matching_language_code_alone_is_not<em>sufficient</em></p>

<p>Azerbaijani (“az”), for example, is written in different scripts depending on
the country in which it is spoken: &ldquo;az-Latn&rdquo; for Latin (the default script),
&ldquo;az-Arab&rdquo; for Arabic, and “az-Cyrl” for Cyrillic.
If you replace &ldquo;az-Arab&rdquo; with just &ldquo;az&rdquo;, the result will be in Latin script and
may not be understandable to a user who only knows the Arabic form.</p>

<p>Also different regions may imply different scripts.
For example: “zh-TW” and “zh-SG” respectively imply the use of Traditional and
Simplified Han. As another example, “sr” (Serbian) defaults to Cyrillic script,
but “sr-RU” (Serbian as written in Russia) implies the Latin script!
A similar thing can be said for Kyrgyz and other languages.</p>

<p>If you ignore subtags, you might as well present Greek to the user.</p>

<p>_The_best_match_might_be_a_language_not_listed_by_the<em>user</em></p>

<p>The most common written form of Norwegian (“nb”) looks an awful lot like Danish.
If Norwegian is not available, Danish may be a good second choice.
Similarly, a user requesting Swiss German (“gsw”) will likely be happy to be
presented German (“de”), though the converse is far from true.
A user requesting Uygur may be happier to fall back to Chinese than to English.
Other examples abound.
If a user-requested language is not supported, falling back to English is often
not the best thing to do.</p>

<p>_The_choice_of_language_decides_more_than<em>translation</em></p>

<p>Suppose a user asks for Danish, with German as a second choice.
If an application chooses German, it must not only use German translations
but also use German (not Danish) collation.
Otherwise, for example, a list of animals might sort “Bär” before “Äffin”.</p>

<p>Selecting a supported language given the user’s preferred languages is like a
handshaking algorithm: first you determine which protocol to communicate in (the
language) and then you stick with this protocol for all communication for the
duration of a session.</p>

<p>_Using_a_“parent”_of_a_language_as_fallback_is<em>non-trivial</em></p>

<p>Suppose your application supports Angolan Portuguese (“pt-AO”).
Packages in [[<a href="http://golang.org/x/text]">http://golang.org/x/text]</a>], like collation and display, may not
have specific support for this dialect.
The correct course of action in such cases is to match the closest parent dialect.
Languages are arranged in a hierarchy, with each specific language having a more
general parent.
For example, the parent of “en-GB-oxendict” is “en-GB”, whose parent is “en”,
whose parent is the undefined language “und”, also known as the root language.
In the case of collation, there is no specific collation order for Portugese,
so the collate package will select the sorting order of the root language.
The closest parent to Angolan Portuguese supported by the display package is
European Portuguese (“pt-PT”) and not the more obvious “pt”, which implies
Brazilian.</p>

<p>In general, parent relationships are non-trivial.
To give a few more examples, the parent of “es-CL” is “es-419”, the parent of
“zh-TW” is “zh-Hant”, and the parent of “zh-Hant” is “und”.
If you compute the parent by simply removing subtags, you may select a “dialect”
that is incomprehensible to the user.</p>

<ul>
<li>Language Matching in Go</li>
</ul>

<p>The Go package [[<a href="http://golang.org/x/text/language]">http://golang.org/x/text/language]</a>] implements the BCP 47
standard for language tags and adds support for deciding which language to use
based on data published in the Unicode Common Locale Data Repository (CLDR).</p>

<p>Here is a sample program, explained below, matching a user&rsquo;s language
preferences against an application&rsquo;s supported languages:</p>

<p>.code -edit matchlang/complete.go</p>

<p>** Creating Language Tags</p>

<p>The simplest way to create a language.Tag from a user-given language code string
is with language.Make.
It extracts meaningful information even from malformed input.
For example, “en-USD” will result in “en” even though USD is not a valid subtag.</p>

<p>Make doesn’t return an error.
It is common practice to use the default language if an error occurs anyway so
this makes it more convenient. Use Parse to handle any error manually.</p>

<p>The HTTP Accept-Language header is often used to pass a user’s desired
languages.
The ParseAcceptLanguage function parses it into a slice of language tags,
ordered by preference.</p>

<p>By default, the language package does not canonicalize tags.
For example, it does not follow the BCP 47 recommendation of eliminating scripts
if it is the common choice in the “overwhelming majority”.
It similarly ignores CLDR recommendations: “cmn” is not replaced by “zh” and
“zh-Hant-HK” is not simplified to “zh-HK”.
Canonicalizing tags may throw away useful information about user intent.
Canonicalization is handled in the Matcher instead.
A full array of canonicalization options are available if the programmer still
desires to do so.</p>

<p>** Matching User-Preferred Languages to Supported Languages</p>

<p>A Matcher matches user-preferred languages to supported languages.
Users are strongly advised to use it if they don’t want to deal with all the
intricacies of matching languages.</p>

<p>The Match method may pass through user settings (from BCP 47 extensions) from
the preferred tags to the selected supported tag.
It is therefore important that the tag returned by Match is used to obtain
language-specific resources.
For example, “de-u-co-phonebk” requests phone-book ordering for German.
The extension is ignored for matching, but is used by the collate package to
select the respective sorting order variant.</p>

<p>A Matcher is initialized with the languages supported by an application, which
are usually the languages for which there are translations.
This set is typically fixed, allowing a matcher to be created at startup.
Matcher is optimized to improve the performance of Match at the expense of
initialization cost.</p>

<p>The language package provides a predefined set of the most commonly used
language tags that can be used for defining the supported set.
Users generally don’t have to worry about the exact tags to pick for supported
languages.
For example, AmericanEnglish (“en-US”) may be used interchangeably with the more
common English (“en”), which defaults to American.
It is all the same for the Matcher. An application may even add both, allowing
for more specific American slang for “en-US”.</p>

<p>** Matching Example</p>

<p>Consider the following Matcher and lists of supported languages:</p>

<pre><code>var supported = []language.Tag{
    language.AmericanEnglish,    // en-US: first language is fallback
    language.German,             // de
    language.Dutch,              // nl
    language.Portuguese          // pt (defaults to Brazilian)
    language.EuropeanPortuguese, // pt-pT
    language.Romanian            // ro
    language.Serbian,            // sr (defaults to Cyrillic script)
    language.SerbianLatin,       // sr-Latn
    language.SimplifiedChinese,  // zh-Hans
    language.TraditionalChinese, // zh-Hant
}
var matcher = language.NewMatcher(supported)
</code></pre>

<p>Let&rsquo;s look at the matches against this list of supported languages for various
user preferences.</p>

<p>For a user preference of &ldquo;he&rdquo; (Hebrew), the best match is &ldquo;en-US&rdquo; (American
English).
There is no good match, so the matcher uses the fallback language (the first in
the supported list).</p>

<p>For a user preference of &ldquo;hr&rdquo; (Croatian), the best match is &ldquo;sr-Latn&rdquo; (Serbian
with Latin script), because, once they are written in the same script, Serbian
and Croatian are mutually intelligible.</p>

<p>For a user preference of &ldquo;ru, mo&rdquo; (Russian, then Moldavian), the best match is
&ldquo;ro&rdquo; (Romanian), because Moldavian is now canonically classified as &ldquo;ro-MD&rdquo;
(Romanian in Moldova).</p>

<p>For a user preference of &ldquo;zh-TW&rdquo; (Mandarin in Taiwan), the best match is
&ldquo;zh-Hant&rdquo; (Mandarin written in Traditional Chinese), not &ldquo;zh-Hans&rdquo; (Mandarin
written in Simplified Chinese).</p>

<p>For a user preference of &ldquo;af, ar&rdquo; (Afrikaans, then Arabic), the best match is
&ldquo;nl&rdquo; (Dutch). Neither preference is supported directly, but Dutch is a
significantly closer match to Afrikaans than the fallback language English is to
either.</p>

<p>For a user preference of &ldquo;pt-AO, id&rdquo; (Angolan Portuguese, then Indonesian), the
best match is &ldquo;pt-PT&rdquo; (European Portuguese), not &ldquo;pt&rdquo; (Brazilian Portuguese).</p>

<p>For a user preference of &ldquo;gsw-u-co-phonebk&rdquo; (Swiss German with phone-book
collation order), the best match is &ldquo;de-u-co-phonebk&rdquo; (German with phone-book
collation order).
German is the best match for Swiss German in the server&rsquo;s language list, and the
option for phone-book collation order has been carried over.</p>

<p>** Confidence Scores</p>

<p>Go uses coarse-grained confidence scoring with rule-based elimination.
A match is classified as Exact, High (not exact, but no known ambiguity), Low
(probably the correct match, but maybe not), or No.
In case of multiple matches, there is a set of tie-breaking rules that are
executed in order.
The first match is returned in the case of multiple equal matches.
These confidence scores may be useful, for example, to reject relatively weak
matches.
They are also used to score, for example, the most likely region or script from
a language tag.</p>

<p>Implementations in other languages often use more fine-grained, variable-scale
scoring.
We found that using coarse-grained scoring in the Go implementation ended up
simpler to implement, more maintainable, and faster, meaning that we could
handle more rules.</p>

<p>** Displaying Supported Languages</p>

<p>The [[<a href="http://golang.org/x/text/language/display]">http://golang.org/x/text/language/display]</a>] package allows naming language
tags in many languages.
It also contains a “Self” namer for displaying a tag in its own language.</p>

<p>For example:</p>

<p>.code -edit matchlang/display.go /START/,/END/</p>

<p>prints</p>

<pre><code>English              (English)
French               (français)
Dutch                (Nederlands)
Flemish              (Vlaams)
Simplified Chinese   (简体中文)
Traditional Chinese  (繁體中文)
Russian              (русский)
</code></pre>

<p>In the second column, note the differences in capitalization, reflecting the
rules of the respective language.</p>

<ul>
<li>Conclusion</li>
</ul>

<p>At first glance, language tags look like nicely structured data, but because
they describe human languages, the structure of relationships between language
tags is actually quite complex.
It is often tempting, especially for English-speaking programmers, to write
ad-hoc language matching using nothing other than string manipulation of the
language tags.
As described above, this can produce awful results.</p>

<p>Go&rsquo;s [[<a href="http://golang.org/x/text/language]">http://golang.org/x/text/language]</a>] package solves this complex problem
while still presenting a simple, easy-to-use API. Enjoy.</p>

  </article>
</section>
</div>

